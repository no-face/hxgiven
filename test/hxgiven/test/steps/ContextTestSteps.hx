package hxgiven.test.steps;

import massive.munit.Assert;

import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;

import hxgiven.Context;
import hxgiven.Reporter;
import hxgiven.model.*;

import hxgiven.test.matchers.CaptureMatcher;

class ContextTestSteps extends hxgiven.BaseSteps<ContextTestSteps> {
	var ctx:Context;

	var stepFunction: StepArgs -> Void;

	var lastStepName:String;
	var stepFunctionCalled:Bool;

	var capturedArgs:StepArgs;
	var expectedArgs:Array<Dynamic>;

	var capturedError:Dynamic;
	var expectedError:Dynamic;

	var captureStepInfo:CaptureMatcher<StepInfo>;
	var captureStepResult:CaptureMatcher<StepResult>;

	public function new() {
		super();
		this.context = mock(Context);

		ctx = new Context();

		lastStepName = null;
		stepFunctionCalled = false;
		capturedArgs = null;

		captureStepInfo = new CaptureMatcher<StepInfo>();
		captureStepResult = new CaptureMatcher<StepResult>();

		stepFunction = buildStepFunction();
	}

	public function a_step_function() {
		stepFunction = buildStepFunction();
	}

	public function a_step_function_that_accepts_some_arguments() {
		given().a_step_function();
	}

	public function a_step_function_that_throws_an_exception(error: Dynamic) {
		expectedError = error;

		stepFunction = buildStepFunction(error);
	}


	public function running_a_step_passing_that_function() {
		runStep();
	}

	public function running_that_step() {
		when().running_a_step_passing_that_function();
	}

	public function running_that_step_passing_an_array_of_arguments( args: Array<Dynamic> ) : Void {
		expectedArgs = args;

		runStep(args);
	}

	public function running_some_step() {
		given().a_step_function();
		when().running_that_step();
	}

	public function running_a_step_that_succeeds(){
		when().running_some_step();
	}


	public function the_function_should_be_called() {
		Assert.isTrue(stepFunctionCalled, "Step function should have been called");
	}

	public function the_function_should_receive_that_arguments() : Void {
		Assert.isNotNull(capturedArgs);
		Assert.areEqual(expectedArgs, capturedArgs.args);
	}

	public function the_exception_should_have_been_rethrown() {
		Assert.isNotNull(capturedError);
		Assert.areEqual(capturedError, expectedError);
	}

	/* **************************** Helpers **************************************/

	function buildStepFunction(?error: Dynamic ){
		return function( args: StepArgs ){
				stepFunctionCalled = true;
				capturedArgs = args;

				if( error != null) {
					throw error;
				}
		};
	}

	public function runStep(?args: Array<Dynamic>, ?name:String) : Void {
		runStepBase(args, name);
	}

	public function runStepBase(args: Array<Dynamic> = null, name:String = "stepName") : Void {
		if( args == null ) {
			args = [];
		}

		this.lastStepName = name;

		try {
			ctx.runStep(name, args, this.stepFunction);
		} catch(e:Dynamic) {
			capturedError = e;
		}
	}

	public function checkCapturedStepInfo( ) : Void {
		stepInfoShouldBe(captureStepInfo.captured, lastStepName, capturedArgs);
	}

	public function checkCapturedStepResult(?expectedError: Dynamic) : Void {
		Assert.isNotNull(captureStepResult.captured);
		stepInfoShouldBe(captureStepResult.captured.stepInfo, lastStepName, capturedArgs);

		if( expectedError != null ) {
			var failure = captureStepResult.captured.failure;

			Assert.isNotNull(failure, "Step failure not captured");
			Assert.areSame(failure.error, expectedError);
		}
	}

	public function stepInfoShouldBe(stepInfo:StepInfo, name:String, args:StepArgs) : Void {
		Assert.isNotNull(stepInfo);
		Assert.areSame(stepInfo.name, name);
		Assert.areSame(stepInfo.args, args);
	}

}
