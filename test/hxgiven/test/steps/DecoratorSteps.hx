package hxgiven.test.steps;

import mockatoo.Mockatoo.*;
import mockatoo.Mockatoo.Matcher;

import hxgiven.model.StepArgs;

class AutoDecoratedSteps implements hxgiven.macros.AutoDecorateSteps {

  public var context:Context;

  public function new(ctx:Context) {
    this.context = ctx;
  }

  @step
  public function stepWithMetadata() : Void {}
}

class DecoratorSteps {

	var ctx:Context;
  var steps:Dynamic;
  var expectedStepName:String;

  public function new() {
    ctx = mock(Context);
    steps = null;
  }

  public function an_AutoDecoratedSteps_subclass_instance( ) : Void {
    steps = new AutoDecoratedSteps(ctx);
  }

  public function calling_a_method_with_step_metadata() : Void {
		this.expectedStepName = "stepWithMetadata";
		this.steps.stepWithMetadata();
	}

	public function the_context_runStep_should_be_called_with_the_method_name() : Void {
		runStepCalledWith(expectedStepName);
	}

  /* ****************************************************************************/

  function runStepCalledWith(?stepName: String, ?args: Dynamic, ?stepFun: Dynamic): Void {
		if( stepName == null ) {
			stepName = this.expectedStepName;
		}
		if( args == null ) {
			args = any;
		}
		if( stepFun == null ) {
			stepFun = any;
		}

		verify(ctx.runStep(stepName, args, stepFun));
	}
}
