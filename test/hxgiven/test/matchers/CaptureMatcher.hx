package hxgiven.test.matchers;

import mockatoo.Mockatoo.Matcher;

class CaptureMatcher<T> {

	public var captured:Null<T>;
	public var all: Array<T>;
	public var nonNullOnly:Bool;

	public function new() {
		this.captured = null;
		this.nonNullOnly = false;
		all = [];
	}

	public function matcher() : Matcher {
		return customMatcher(function ( value: Dynamic ) : Bool {
			var v = (value : T);
			this.captured = v;

			all.push(v);

			return (if(nonNullOnly) value != null else true );
		});
	}
}
