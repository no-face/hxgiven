package hxgiven.test.stubs;

import hxgiven.BaseSteps;

class StepsStub extends BaseSteps<StepsStub> {

  public function new() {
    super();
  }

  @step
  public function some_step() : Void {

  }
}
