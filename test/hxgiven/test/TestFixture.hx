package hxgiven.test;

import haxe.Constraints;
import hxgiven.BaseFixture;

@:generic
class TestFixture<T:Constructible<Void->Void>> extends BaseFixture<T>{
  override public function keyword( k: hxgiven.model.Keywords) : T {
    //Does not notify Context about keyword

    return super.getInstance();
  }
}
