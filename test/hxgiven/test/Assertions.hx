package hxgiven.test;

import massive.munit.Assert;

import haxe.PosInfos;

class Assertions   {
  public static
  function arraysAreEquals<T>(
    expected: Array<T>, actual: Array<T>,
    ?equalsFn: T->T->Bool, ?message:String, ?info:PosInfos) : Void
  {
		Assert.isNotNull(expected, "Expected array is null!");
		Assert.isNotNull(actual,   "Array is null!");

		var defaultMessage:String;

		if( expected.length != actual.length ) {
				defaultMessage = 'Array ${actual} have a different size than expected ${expected}';
		}
		else {
			var diffIndex = -1;

			for( i in 0...expected.length ) {
          var equals = (expected[i] == actual[i]);

          if(!equals && equalsFn != null) {
            equals = equalsFn(expected[i], actual[i]);
          }

					if(!equals) {
						diffIndex = i;
						break;
					}
			}
			if( diffIndex < 0 ) {
				return; //equals
			}

			defaultMessage = 'Arrays ${actual} differed from expected ${expected} at index ${diffIndex}';
		}

		if( message == null ) {
			message = defaultMessage;
		}

		Assert.fail(message, info);
	}

}
