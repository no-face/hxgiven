package hxgiven.test.samples;

import massive.munit.Assert;

class BasicTest extends hxgiven.test.TestFixture<BasicSteps> {

  @Test
  @Scenario
  public function basic_scenario() : Void {
    Assert.isNotNull(given());

    given().an_initial_condition();
    when().something_happens();
    then().a_result_is_expected();
  }
}

class BasicSteps  extends hxgiven.BaseSteps<BasicSteps>{
  @Step
  public function an_initial_condition() : Void {}

  @Step
  public function something_happens() : Void {}

  @Step
  public function a_result_is_expected() : Void {}
}
