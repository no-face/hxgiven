package hxgiven.test.tests;

import massive.munit.Assert;
import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;

import hxgiven.test.Assertions;
import hxgiven.test.matchers.CaptureMatcher;

import hxgiven.report.NamesFormatter;
import hxgiven.report.Printer;
import hxgiven.report.ReportLevel;
import hxgiven.report.ResultPrinter;
import hxgiven.model.*;
import hxgiven.model.Keywords;

class ResultPrinterTest extends hxgiven.test.TestFixture<ResultPrinterSteps>{
    static var CompleteScenario:ScenarioResultArgs = {
      name: "example_scenario",
      steps: [
        [
          Given, "a_condition", null,
          [
            [ Given, "an_internal_condition"],
            [ When, "an_internal_step_occurs"]
          ]
        ],
        [ When, "something_happens"],
        [ Then, "a_result_is_expected"]
      ]
    };

    static var FAILED_SCENARIO_WITH_STEPS:ScenarioResultArgs = {
      name: "example_scenario",
      failure: new Failure("scenario failure"),
      steps: [
        [ Given, "a_condition"],
        [ When, "something_happens"],
        [ Then, "an_error_occur", new Failure("error"), [
            [Then, "an_internal_test"],
            [Then, "an internal error", new Failure("internal error")]
        ]]
      ]
    };

    static var TAB = '  ';

    static var SCENARIO_TITLE_MESSAGE = "Scenario: example scenario";
    static var SCENARIO_GIVEN_MESSAGE = '${TAB}given a condition';
    static var SCENARIO_WHEN_MESSAGE  = '${TAB}when something happens';
    static var SCENARIO_THEN_MESSAGE  = '${TAB}then a result is expected';
    static var THEN_ERROR_MESSAGE     = '${TAB}then an error occur';

    static var GIVEN_INTERNAL_MESSAGE = '${TAB}${TAB}given an internal condition';
    static var WHEN_INTERNAL_MESSAGE  = '${TAB}${TAB}when an internal step occurs';
    static var THEN_INTERNAL_TEST_MESSAGE  = '${TAB}${TAB}then an internal test';
    static var THEN_INTERNAL_ERROR_MESSAGE  = '${TAB}${TAB}then an internal error';

    static var CompleteScenario_messages = [
      SCENARIO_TITLE_MESSAGE,
      SCENARIO_GIVEN_MESSAGE,
      SCENARIO_WHEN_MESSAGE,
      SCENARIO_THEN_MESSAGE
    ];


    static var CompleteScenario_full_messages = [
      SCENARIO_TITLE_MESSAGE,
      SCENARIO_GIVEN_MESSAGE,
        GIVEN_INTERNAL_MESSAGE,
        WHEN_INTERNAL_MESSAGE,
      SCENARIO_WHEN_MESSAGE,
      SCENARIO_THEN_MESSAGE
    ];

    static var FAILED_SCENARIO_NEUTRAL_MESSAGES = [
      SCENARIO_GIVEN_MESSAGE,
      SCENARIO_WHEN_MESSAGE
    ];

    static var FAILED_SCENARIO_FULL_NEUTRAL_MESSAGES = [
      SCENARIO_GIVEN_MESSAGE,
      SCENARIO_WHEN_MESSAGE,
      THEN_INTERNAL_TEST_MESSAGE
    ];

    static var FAILED_SCENARIO_ERROR_MESSAGES = [
      THEN_ERROR_MESSAGE
    ];
    static var FAILED_SCENARIO_FULL_ERROR_MESSAGES = [
      THEN_ERROR_MESSAGE,
      THEN_INTERNAL_ERROR_MESSAGE
    ];


    @Before
    public function setup(){
      super.resetSteps();
    }

    @Test
    public function print_successful_empty_scenario() {
      given().a_ResultPrinter_instance_with_a_valid_Printer();
      given().the_report_level_is_greater_or_equal_to(Summary);
      when().printing_a_ScenarioResult({ name: "example_scenario"});
      then().it_should_print_success_messages([
        "Scenario: example scenario"
      ]);
    }

    @Test
    public function print_failed_empty_scenario() {
      given().a_ResultPrinter_instance_with_a_valid_Printer();
      given().the_report_level_is_greater_or_equal_to(Minimum);
      when().printing_a_ScenarioResult({
        name: "example_scenario",
        failure: new Failure("error")
      });
      then()
        .it_should_print_error_messages([
          "Scenario: example scenario"
        ]);
        and().it_should_print_a_failure(new Failure("error"));
    }

    @Test
    public function print_detailed_successful_scenario_with_steps() {
      given().a_ResultPrinter_instance_with_a_valid_Printer();
      given().the_report_level_is_set_to(Detailed);

      when().printing_a_ScenarioResult(CompleteScenario);
      then().it_should_print_only_these_success_messages(CompleteScenario_messages);
      and().it_should_print_a_separator_to_the_next_scenario();
    }

    @Test
    public function print_detailed_failed_scenario_with_steps() {
      given().a_ResultPrinter_instance_with_a_valid_Printer();
      given().the_report_level_is_set_to(Detailed);

      when().printing_a_ScenarioResult(FAILED_SCENARIO_WITH_STEPS);

      then()
        .it_should_print_the_failed_scenario_with_details(
          SCENARIO_TITLE_MESSAGE,
          FAILED_SCENARIO_NEUTRAL_MESSAGES,
          FAILED_SCENARIO_ERROR_MESSAGES,
          new Failure("scenario failure")
        );
    }


      @Test
      public function print_successful_scenario_with_internal_steps_at_full_report_level() {
        given().a_ResultPrinter_instance_with_a_valid_Printer();
        given().the_report_level_is_set_to(Full);

        when().printing_a_ScenarioResult(CompleteScenario);
        then().it_should_print_success_messages(CompleteScenario_full_messages);
        and().it_should_print_a_separator_to_the_next_scenario();
      }

      @Test
      public function print_failed_scenario_with_internal_steps_at_full_report_level() {
        given().a_ResultPrinter_instance_with_a_valid_Printer();
        given().the_report_level_is_set_to(Full);

        when().printing_a_ScenarioResult(FAILED_SCENARIO_WITH_STEPS);

        then()
          .it_should_print_the_failed_scenario_with_details(
            SCENARIO_TITLE_MESSAGE,
            FAILED_SCENARIO_FULL_NEUTRAL_MESSAGES,
            FAILED_SCENARIO_FULL_ERROR_MESSAGES,
            new Failure("scenario failure")
          );
      }

    @Test
    public function print_summary_of_successful_scenario() {
      given().a_ResultPrinter_instance_with_a_valid_Printer();
      given().the_report_level_is_set_to(Summary);
      when().printing_a_ScenarioResult(CompleteScenario);
      then().it_should_print_only_these_success_messages([
        SCENARIO_TITLE_MESSAGE
      ]);
    }

    @Test
    public function print_successful_scenario_at_minimum_report_level() {
      given().a_ResultPrinter_instance_with_a_valid_Printer();
      given().the_report_level_is_set_to(Minimum);
      when().printing_a_ScenarioResult(CompleteScenario);
      then().it_should_not_print_any_message();
    }

    @Test
    public function print_failed_scenario_at_minimum_report_level() {
      given().a_ResultPrinter_instance_with_a_valid_Printer();
      given().the_report_level_is_greater_or_equal_to(Minimum);
      when().printing_a_ScenarioResult(FAILED_SCENARIO_WITH_STEPS);

      then()
        .it_should_print_the_failed_scenario_with_details(
          SCENARIO_TITLE_MESSAGE,
          FAILED_SCENARIO_NEUTRAL_MESSAGES,
          FAILED_SCENARIO_ERROR_MESSAGES,
          new Failure("scenario failure")
        );
    }

    @Test
    public function print_successful_scenario_at_quiet_report_level() {
      given().a_ResultPrinter_instance_with_a_valid_Printer();
      given().the_report_level_is_set_to(Quiet);
      when().printing_a_ScenarioResult(CompleteScenario);
      then().it_should_not_print_any_message();
    }

    @Test
    public function print_failed_scenario_at_quiet_report_level() {
      given().a_ResultPrinter_instance_with_a_valid_Printer();
      given().the_report_level_is_set_to(Quiet);
      when().printing_a_ScenarioResult(FAILED_SCENARIO_WITH_STEPS);
      then().it_should_not_print_any_message();
    }
}

typedef ScenarioResultArgs = {
  ?name: String,
  ?formattedName: String,
  ?failure: Failure,
  ?status: ProgressStatus,
  ?steps: Array<Array<Dynamic>>
};

class ResultPrinterSteps extends hxgiven.BaseSteps<ResultPrinterSteps>{
  var resultPrinter: ResultPrinter;
  var printer: Printer;
  var namesFormatter: NamesFormatter;

  public function new() {
    super();

    namesFormatter = new NamesFormatter();
  }

  public function a_ResultPrinter_instance_with_a_valid_Printer() {
    printer = mock(Printer);
    resultPrinter = new ResultPrinter(printer);
  }

  public function the_report_level_is_set_to( level: ReportLevel ) {
    resultPrinter.level = level;
  }
  public function the_report_level_is_greater_or_equal_to( level: ReportLevel ) {
    given().the_report_level_is_set_to(level);
  }

  public function printing_a_ScenarioResult( scenario: ScenarioResultArgs ) : Void {
    Assert.isNotNull(scenario);

    resultPrinter.printResult(buildScenarioResult(scenario));
  }

  public function it_should_print_a_warning( lines: Array<String> ) : Void {
    Mockatoo.verify(printer.warning(linesToText(lines)));
  }

  public function it_should_print_success_messages( messages: Array<String>, ?mode: VerificationMode ) {
    for( m in messages ) {
      Mockatoo.verify(printer.success(m));
    }
  }

  public function it_should_print_only_these_success_messages( messages: Array<String> ) : Void {
    var captureMessages = new CaptureMatcher<String>();

    Mockatoo.verify(printer.success(captureMessages.matcher()), atLeastOnce);

    checkMessages(messages, captureMessages.all);
  }

  public function it_should_print_neutral_messages( messages: Array<String> ) : Void {
    for( m in messages ) {
      Mockatoo.verify(printer.info(m));
    }
  }

  public function it_should_print_only_these_neutral_messages( messages: Array<String> ) : Void {
    var captureMessages = new CaptureMatcher<String>();

    Mockatoo.verify(printer.info(captureMessages.matcher()), atLeastOnce);

    checkMessages(messages, captureMessages.all);
  }

  public function it_should_print_error_messages( messages: Array<String> ) : Void {
    for( m in messages ) {
      Mockatoo.verify(printer.error(m));
    }
  }

  public function it_should_print_only_these_error_messages( messages: Array<String> ) : Void {
      var captureMessages = new CaptureMatcher<String>();

      Mockatoo.verify(printer.error(captureMessages.matcher()), atLeastOnce);

      checkMessages(messages, captureMessages.all);
  }

  public function it_should_print_a_failure( f: Failure, ?times: mockatoo.VerificationMode ) : Void {
    var captureFailure = new CaptureMatcher<Failure>();
    captureFailure.nonNullOnly = true;

    Mockatoo.verify(printer.failure(captureFailure.matcher()), times);

    Assert.areEqual(f.error, captureFailure.captured.error);
  }

  public function it_should_print_only_one_failure( f: Failure ) : Void {
    then().it_should_print_a_failure(f, times(1));
  }

  public function it_should_print_a_separator_to_the_next_scenario() {
    Mockatoo.verify(printer.line());
  }

  public function it_should_not_print_any_message() {
    Mockatoo.verify(printer.success(any), never);
    Mockatoo.verify(printer.error(any)  , never);
    Mockatoo.verify(printer.failure(any), never);
    Mockatoo.verify(printer.warning(any), never);
    Mockatoo.verify(printer.info(any)   , never);
    Mockatoo.verify(printer.line()      , never);
  }

  public function it_should_print_the_failed_scenario_with_details(
    title: String,
    neutralMessages: Array<String>,
    errorMessages: Array<String>,
    failure: Failure ) : Void
  {
    it_should_print_error_messages([
      title
    ]);
    and().it_should_print_only_these_neutral_messages(neutralMessages);
    and().it_should_print_only_these_error_messages(errorMessages);
    and().it_should_print_only_one_failure(failure);
    and().it_should_print_a_separator_to_the_next_scenario();
  }

  /****************************************************************************/

  function checkMessages( expected: Array<String>, captured: Array<String> ) : Void {
    Assertions.arraysAreEquals(expected, captured,
      '\n' +
      'Should have print:\n' + expected.join('\n') + '\nBut was print:\n' + captured.join('\n')
      + '\n');
  }

  function buildScenarioResult( scenario: ScenarioResultArgs ) : ScenarioResult {
    var info = new ScenarioInfo(scenario.name, formatName(scenario.formattedName, scenario.name));
    var result = new ScenarioProgress(info);
    if( scenario.status == null || scenario.failure != null) {
      result.finished(scenario.failure);
    }
    if(scenario.status != null){
      result.status = scenario.status;
    }

    if( scenario.steps != null ) {
      for( step in scenario.steps ) {
        result.addStep(buildStepResult(step));
      }

    }

    return result;
  }

  function buildStepResult( stepResultArgs: Array<Dynamic>) : StepResult {
    var keyword = stepResultArgs[0];
    var name = stepResultArgs[1];
    var failure = if(stepResultArgs.length >= 3) stepResultArgs[2] else null;

    var step = new StepProgress(new StepInfo(keyword, name, new StepArgs([]), formatName(null, name)));

    if( stepResultArgs.length >= 4 ) {
      var internalSteps = (stepResultArgs[3] : Array<Dynamic>);
      for( internal in  internalSteps) {
        step.addInternalStep(buildStepResult(internal));
      }
    }

    step.finish(failure);

    return step;
  }

  function formatName( formattedName:String, name:String ) : String {
    if( formattedName != null ) {
      return formattedName;
    }

    return namesFormatter.format(name);
  }

  function linesToText(lines: Array<String>): String {
    return lines.join('\n');
  }
}
