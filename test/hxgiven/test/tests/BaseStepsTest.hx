package hxgiven.test.tests;

import massive.munit.Assert;
import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;

import hxgiven.BaseSteps;
import hxgiven.Context;
import hxgiven.model.Keywords;

import hxgiven.test.stubs.*;
import hxgiven.test.matchers.CaptureMatcher;

class BaseStepsTest
{
	var step:StepsStub;

	public function new()
	{}

	@Before
	public function setup(){
		step = new StepsStub();
		step.context = mock(Context);
	}

	@Test
	public function testExample(){
		Assert.isNotNull(step.context);
		Assert.isType(step.context, Context);
	}

	@Test
	public function keyword_methods_should_allow_chaining_calls( ) : Void {
		checkKeywordReturn(step.given());
		checkKeywordReturn(step.when());
		checkKeywordReturn(step.then());
		checkKeywordReturn(step.and());
		checkKeywordReturn(step.but());
	}

	@Test
	public function keyword_methods_should_notify_the_used_keyword_to_the_context() : Void {
		checkNotifiedKeyword(step.given(), Given);
		checkNotifiedKeyword(step.when() , When);
		checkNotifiedKeyword(step.then() , Then);
		checkNotifiedKeyword(step.and()  , And);
		checkNotifiedKeyword(step.but()  , But);
	}

	function checkKeywordReturn( returned: Dynamic ) : Void {
		Assert.areSame(step, returned);
		Assert.isType(returned, StepsStub);
	}

	function checkNotifiedKeyword( ignore: Dynamic, keyword: Keywords ) : Void {
		var matcher = new CaptureMatcher<Keywords>();
		Mockatoo.verify(step.context.stepKeyword(matcher.matcher()));

		Assert.areEqual(keyword, matcher.captured,
			'Captured keyword (${matcher.captured}) is not the expected (${keyword})');
	}
}
