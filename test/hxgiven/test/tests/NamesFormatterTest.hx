package hxgiven.test.tests;

import massive.munit.Assert;

import hxgiven.report.NamesFormatter;

class NamesFormatterTest{
    var formatter: NamesFormatter;

    @Before
    public function setup() {
      formatter = new NamesFormatter();
    }

    @Test
    public function snake_case_format(){
      testFormat("my_scenario_example", "my scenario example");
      testFormat("snake_With_UpperCaseLetters", "snake With UpperCaseLetters");
      testFormat("double__underscores", "double  underscores");
      testFormat("_initial_underscores", "initial underscores");
      testFormat("final_underscores_", "final underscores");
    }

    @Test
    public function cammel_case_format(){
      testFormat("simple", "simple");
      testFormat("myScenarioExample", "my scenario example");
      testFormat("CamelCaseStartUppercase", "Camel case start uppercase");
    }

    function testFormat(input: String, expected: String) : Void {
      var result = formatter.format(input);
      Assert.areEqual(expected, result,
        '"${input}" formatted should be "${expected}", but was: "${result}"');
    }
}
