package hxgiven.test.tests.macros;

import hxgiven.test.steps.DecoratorSteps;

class AutoDecoratedStepsTest{

  var steps:DecoratorSteps;

  public function new() {}

  public function given(){ return this.steps; }
  public function when() { return this.steps; }
  public function then() { return this.steps; }

  @Before
  public function setup(  ) {
    steps = new DecoratorSteps();
  }

  @Test
  public function should_decorate_all_step_methods_of_implementing_subclasses() : Void {
    given().an_AutoDecoratedSteps_subclass_instance();
    when().calling_a_method_with_step_metadata();
    then().the_context_runStep_should_be_called_with_the_method_name();
  }
}
