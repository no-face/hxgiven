package hxgiven.test.tests.macros;

import massive.munit.Assert;
import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;

import hxgiven.test.matchers.CaptureMatcher;

import hxgiven.Context;
import hxgiven.Reporter;
import hxgiven.model.*;
import hxgiven.macros.ScenariosDecorator;

class ScenariosDecoratorTest {
	var steps: ScenariosDecoratorSteps;

	public function new()
	{}

	public function given(){ return this.steps; }
	public function when() { return this.steps; }
	public function then() { return this.steps; }
	public function and()  { return this.steps; }

	@Before
	public function setup(){
		steps = new ScenariosDecoratorSteps();
	}

	@Test
	public function decorated_scenario_starts(){
		given().a_test_class_instance_with_a_valid_context_field();
		when().calling_a_decorated_scenario_method();
		then().the_context_scenarioStarted_method_should_be_called_once_with_a_valid_ScenarioInfo();
		and().the_received_ScenarioInfo_should_store_the_scenario_method_name();
	}

	@Test
	public function decorated_scenario_succeeds(){
		given().a_test_class_instance_with_a_valid_context_field();
		when().a_decorated_scenario_method_from_that_class_succeeds();
		then().the_context_should_be_notified_about_the_scenario_success();
	}

	@Test
	public function decorated_scenario_fails(){
		given().a_test_class_instance_with_a_valid_context_field();
		when().a_decorated_scenario_method_from_that_class_throws_an_exception();
		then().the_context_should_be_notified_about_the_scenario_failure();
		and().the_exception_should_have_been_thrown();
	}

	@Test
	public function decorated_scenario_with_return(){
		given().a_test_class_instance_with_a_valid_context_field();
		when().a_decorated_scenario_method_returns_early();
		then().the_context_method_scenarioFinished_should_still_be_called();
	}

	@Test
	public function auto_decorated_scenario() {
		given().a_instance_of_an_auto_decorated_scenario_class();
		given().that_instance_has_a_valid_context_field();
		when().calling_a_method_with_scenario_metadata();
		then().that_method_should_have_been_decorated_automatically();
	}


	@Test
	public function only_auto_decorate_scenarios_with_tag() {
		given().a_instance_of_an_auto_decorated_scenario_class();
		given().that_instance_has_a_valid_context_field();
		when().calling_a_method_without_scenario_metadata();
		then().that_method_should_not_have_been_decorated_automatically();
	}
}


class ScenariosDecoratorSteps {

	var testClass:Dynamic;
	var ctx:Context;
	var capturedScenarioInfo:CaptureMatcher<ScenarioInfo>;
	var capturedFailure     :CaptureMatcher<Failure>;
	var capturedException   :Dynamic;
	var expectedScenarioName:String;

	public function new() {
		ctx = mock(Context);
		capturedScenarioInfo = new CaptureMatcher<ScenarioInfo>();
		capturedFailure = new CaptureMatcher<Failure>();
		capturedFailure.nonNullOnly = false;
	}

	public function a_test_class_instance_with_a_valid_context_field( ) : Void {
		testClass = new DecoratedTestClass(ctx);
	}

	public function calling_a_decorated_scenario_method( ) : Void {
		testClass.decoratedScenarioMethod();

		expectedScenarioName = "decoratedScenarioMethod";
	}

	public function a_decorated_scenario_method_from_that_class_succeeds() : Void {
		calling_a_decorated_scenario_method();
	}

	public function a_decorated_scenario_method_from_that_class_throws_an_exception() : Void {
		expectedScenarioName = "failed_scenario";

		try {
			testClass.failed_scenario();
		} catch(e:Dynamic) {
			this.capturedException = e;
		}
	}

	public function a_decorated_scenario_method_returns_early() : Void {
		expectedScenarioName = "returns_early_scenario";
		testClass.returns_early_scenario();
	}

	public function the_context_scenarioStarted_method_should_be_called_once_with_a_valid_ScenarioInfo( ){
		verify(ctx.scenarioStarted(capturedScenarioInfo.matcher()), times(1));

		Assert.isNotNull(capturedScenarioInfo.captured);
	}

	public function the_context_should_be_notified_about_the_scenario_success() {
		verifyScenarioFinishedCall();

		Assert.isNull(capturedFailure.captured);
	}

	public function the_context_method_scenarioFinished_should_still_be_called() : Void {
		the_context_should_be_notified_about_the_scenario_success();
	}

	public function the_context_should_be_notified_about_the_scenario_failure() {
		verifyScenarioFinishedCall();

		Assert.isNotNull(capturedFailure.captured);
	}

	public function the_received_ScenarioInfo_should_store_the_scenario_method_name(  ) {
		Assert.areEqual(expectedScenarioName, capturedScenarioInfo.captured.name);
	}

	public function the_exception_should_have_been_thrown() : Void {
		Assert.isNotNull(capturedException, "No exception was captured");
	}

	/****************************************************************************/

	//Given
	public function a_instance_of_an_auto_decorated_scenario_class() {
		testClass = new AutoDecoratedScenarioClass();
	}

	public function that_instance_has_a_valid_context_field() {
		testClass.context = ctx;
	}

	//When
	public function calling_a_method_with_scenario_metadata() {
		expectedScenarioName = "decorated_method";
		testClass.decorated_method();
	}

	public function calling_a_method_without_scenario_metadata() {
		testClass.non_decorated_method();
	}

	//Then
	public function that_method_should_have_been_decorated_automatically() {
		the_context_scenarioStarted_method_should_be_called_once_with_a_valid_ScenarioInfo();
		the_received_ScenarioInfo_should_store_the_scenario_method_name();
	}

	public function that_method_should_not_have_been_decorated_automatically() {
		verify(ctx.scenarioStarted(any), never);
	}

	/****************************************************************************/

	function verifyScenarioFinishedCall() : Void {
		verify(ctx.scenarioFinished(capturedFailure.matcher()));
	}
}

@:build(hxgiven.macros.ScenariosDecorator.decorateMethod("decoratedScenarioMethod"))
@:build(hxgiven.macros.ScenariosDecorator.decorateMethod("failed_scenario"))
@:build(hxgiven.macros.ScenariosDecorator.decorateMethod("returns_early_scenario"))
@:build(hxgiven.macros.ScenariosDecorator.decorateMethod("with_args"))
class DecoratedTestClass{
	public var context:Context;

	public function new(ctx:Context) {
		this.context = ctx;
	}

	public function decoratedScenarioMethod() : Void {}

	public function failed_scenario() : Void {
		throw "Error";
	}

	public function returns_early_scenario() : Void {
			return;
	}

	public function with_args(s: String) : Void {
			trace(s);
	}
}


class AutoDecoratedScenarioClass implements hxgiven.macros.AutoDecorateScenario{
	public var context:Context;

	public function new() {
	}

	@scenario
	public function decorated_method() : Void {

	}

	public function non_decorated_method() : Void {

	}
}
