package hxgiven.test.tests.macros;

import massive.munit.Assert;
import mockatoo.Mockatoo.*;
import mockatoo.Mockatoo.Matcher;

import hxgiven.Context;
import hxgiven.model.StepArgs;
import hxgiven.model.ScenarioInfo;
import hxgiven.macros.StepsDecorator;

import hxgiven.test.Assertions;
import hxgiven.test.matchers.CaptureMatcher;

import haxe.PosInfos;

@:build(hxgiven.macros.StepsDecorator.decorateMethod("voidStep"))
@:build(hxgiven.macros.StepsDecorator.decorateMethod("valueStep"))
@:build(hxgiven.macros.StepsDecorator.decorateMethod("stepWithBody"))
@:build(hxgiven.macros.StepsDecorator.decorateMethod("stepStoreArgs"))
@:build(hxgiven.macros.StepsDecorator.decorateAll())
class TestSteps {

	public var context:Context;

	public var calledStepWithBody:Int=0;
	public var storedArgs:Array<Array<Int>>;

	public function new(ctx:Context) {
		this.context = ctx;
		this.storedArgs = [];
	}

	public function voidStep() : Void {}

	public function valueStep(v: Dynamic) : Void {}

	public function stepWithBody() : Void {
		++calledStepWithBody;
	}

	public function stepStoreArgs( a:Int, b:Int, c:Int ) : Void {
		this.storedArgs.push([a, b, c]);
	}

	@step
	public function stepWithMetadata( ) : Void {}
}



class StepsDecoratorTest
{
	var ctx:Context;
	var steps:TestSteps;
	var expectedStepName:String;
	var captureParameters: CaptureMatcher<ArgsList>;
	var captureStepFunction: CaptureMatcher<StepFunction>;

	public function new()
	{}

	@Before
	public function setup(){
		ctx = mock(Context);
		steps = new TestSteps(ctx);

		expectedStepName = null;

		captureParameters = new CaptureMatcher<ArgsList>();
		captureStepFunction = new CaptureMatcher<StepFunction>();
	}

	/* **************************** Decorate method *****************************/

	@Test
	public function decorated_method__should_call_runStep_with_the_method_name(){
		steps.voidStep();

		verify(ctx.runStep("voidStep", any, any));
	}

	@Test
	public function decorated_method__should_pass_parameters_as_array_to_runStep(){
		var expectedValues:Array<Dynamic> = ["Example"];

		steps.valueStep(expectedValues[0]);

		verify(ctx.runStep("valueStep", captureParameters.matcher(), any));

		Assertions.arraysAreEquals(expectedValues, captureParameters.captured);
	}

	@Test
	public function decorated_method__should_pass_its_body_as_a_function_to_runStep(){
		steps.stepWithBody();

		verify(ctx.runStep("stepWithBody", any, captureStepFunction.matcher()));

		var f = captureStepFunction.captured;
		f(new StepArgs([]));

		Assert.areEqual(1, steps.calledStepWithBody, "Function body should be called");
	}

	@Test
	public function decorated_method__should_pass_parameters_values_to_function_body__when_called(){
		steps.stepStoreArgs(1, 2, 3);

		verify(ctx.runStep("stepStoreArgs", any, captureStepFunction.matcher()));

		var f = captureStepFunction.captured;
		f(new StepArgs([1, 2, 3]));
		f(new StepArgs([4, 5, 6]));

		Assertions.arraysAreEquals([1, 2, 3], steps.storedArgs[0]);
		Assertions.arraysAreEquals([4, 5, 6], steps.storedArgs[1]);
	}

  /* **************************** Decorate class *****************************/

	@Test
	public function should_decorate_all_methods_with_steps_metadata_of_a_decorated_class() : Void {
		given_a_decorated_class_instance();
		when_calling_a_method_with_step_metadata();
		then_the_context_runStep_should_be_called_with_the_method_name();
	}


  /* ****************************** Steps *************************************/

	function given_a_decorated_class_instance( ) : Void {}

	function when_calling_a_method_with_step_metadata() : Void {
		this.expectedStepName = "stepWithMetadata";
		this.steps.stepWithMetadata();
	}

	function then_the_context_runStep_should_be_called_with_the_method_name() : Void {
		runStepCalledWith(expectedStepName);
	}

	/* ***************************************************************************/

	function runStepCalledWith(?stepName: String, ?args: Dynamic, ?stepFun: Dynamic): Void {
		if( stepName == null ) {
			stepName = this.expectedStepName;
		}
		if( args == null ) {
			args = any;
		}
		if( stepFun == null ) {
			stepFun = any;
		}

		verify(ctx.runStep(stepName, args, stepFun));
	}
}
