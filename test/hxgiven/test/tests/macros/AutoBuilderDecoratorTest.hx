package hxgiven.test.tests.macros;

import massive.munit.Assert;

class AutoBuilderDecoratorTest extends hxgiven.test.TestFixture<AutoBuilderDecoratorSteps>{

  @Test
  public function instantiate_autobuild_field() {
    given().a_decorated_class_with_an_autobuild_field();
    when().acessing_that_field();
    then().it_should_be_automatically_instantiated();
  }

  @Test
  public function set_autobuild_field_value() {
    given().a_decorated_class_with_an_autobuild_field();
    when().setting_the_value_of_that_field();
    and().acessing_that_field();
    then().the_set_value_should_be_returned();
  }

  @Test
  public function reset_autobuild_field() {
    given().a_decorated_class_with_an_autobuild_field();
    when().setting_that_field_to_null();
    and().acessing_that_field();
    then().a_new_value_should_be_instantiated_automatically();
  }
}

class AutoBuilderDecoratorSteps extends BaseSteps<AutoBuilderDecoratorSteps> {

  var instance:DecoratedStepBuilder;
  var capturedField:AutoBuilded;
  var expectedValue:AutoBuilded;

  public function new() {
    super();
  }

  public function a_decorated_class_with_an_autobuild_field(  ) {
    instance = new DecoratedStepBuilder();
  }

  public function acessing_that_field(  ) {
    capturedField = instance.autobuilded;
  }

  public function setting_the_value_of_that_field() {
    expectedValue = new AutoBuilded();
    instance.autobuilded = expectedValue;
  }

  public function setting_that_field_to_null() {
    instance.autobuilded = null;
  }

  public function it_should_be_automatically_instantiated(  ) {
    Assert.isNotNull(capturedField, "AutoBuilded field should not be null");
  }

  public function the_set_value_should_be_returned() {
    Assert.areSame(expectedValue, capturedField);
  }

  public function a_new_value_should_be_instantiated_automatically() {
    then().it_should_be_automatically_instantiated();
  }
}

@:build(hxgiven.macros.AutoBuilderDecorator.decorateAll())
class DecoratedStepBuilder{

  @autobuild
  public var autobuilded:AutoBuilded;

  public function new() {}
}

class AutoBuilded {

  public function new() {
  }
}
