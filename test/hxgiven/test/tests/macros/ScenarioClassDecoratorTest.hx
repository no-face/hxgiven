package hxgiven.test.tests.macros;

import massive.munit.Assert;
import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;

using haxe.EnumTools;
using haxe.EnumTools.EnumValueTools;

import haxe.Constraints; //Function

import hxgiven.Context;
import hxgiven.model.Keywords;

import hxgiven.test.matchers.CaptureMatcher;
import hxgiven.test.Assertions;

class ScenarioClassDecoratorTest extends hxgiven.test.TestFixture<ScenarioClassDecoratorSteps>{

  @Test
  public function should_autobuild_steps_field() {
    given().a_decorated_scenario_class_instance_with_a_steps_field();
    when().acessing_that_field();
    then().it_should_be_automatically_instantiated();
  }

  @Test
  public function should_create_keyword_methods() {
    given().a_decorated_scenario_class_instance_with_a_steps_field();
    then().it_should_have_the_keyword_methods();
  }

  @Test
  public function should_return_the_steps_field_for_each_keyword_method() {
    given().a_decorated_scenario_class_instance_with_a_steps_field();
    when().calling_each_keyword_method();
    then().they_should_return_the_steps_field();
  }

  @Test
  public function should_notify_the_context_about_the_executed_keyword_methods() {
    given().a_decorated_scenario_class_instance_with_a_steps_field();
    given().that_instance_has_a_non_null_context_field();
    when().calling_each_keyword_method();
    then().the_context_should_be_notified_about_each_keyword_call();
  }

  @Test
  public function should_create_a_method_to_reset_the_steps_field() {
    given().a_decorated_scenario_class_instance_with_a_steps_field();
    then().it_should_have_a_method_resetSteps();
  }

  @Test
  public function reset_steps_should_recreate_the_steps_field() {
    given().a_decorated_scenario_class_instance_with_a_steps_field();
    given().the_steps_field_was_acessed_before();
    when().calling_reset_steps();
    then().the_steps_field_should_have_another_value();
  }
}

class ScenarioClassDecoratorSteps extends BaseSteps<ScenarioClassDecoratorSteps> {
  var decoratedInstance:DecoratedScenarioClass;
  var capturedSteps:SampleSteps;
  var capturedCallReturns:Map<String, Dynamic>;

  public function new() {
    super();

    capturedCallReturns = new Map<String, Dynamic>();
  }

  public function a_decorated_scenario_class_instance_with_a_steps_field() {
    decoratedInstance = new DecoratedScenarioClass();
  }

  public function that_instance_has_a_non_null_context_field() {
    decoratedInstance.context = mock(Context);
  }

  public function the_steps_field_was_acessed_before() {
    when().acessing_that_field();
  }

  public function acessing_that_field() {
    Assert.isNotNull(decoratedInstance);

    capturedSteps = decoratedInstance.steps;
  }

  public function calling_each_keyword_method() {
    for( keyword in Keywords.getConstructors() ) {
      if( keyword != "Custom" ) {
        var methodName = keyword.toLowerCase();
        var method:Function = checkMethod(decoratedInstance, methodName);

        var result = Reflect.callMethod(decoratedInstance, method, []);

        capturedCallReturns[keyword] = result;
      }
    }
  }

  public function calling_reset_steps() {
    var method = checkMethod(decoratedInstance, "resetSteps");
    Reflect.callMethod(decoratedInstance, method, []);
  }

  public function it_should_be_automatically_instantiated() {
    Assert.isNotNull(capturedSteps, "steps field should not be null");
  }

  public function it_should_have_the_keyword_methods() {
    for( item in Keywords.getConstructors() ) {
      if( item != "Custom" ) {
        hasKeywordMethod(decoratedInstance, item.toLowerCase());
      }
    }
  }

  public function they_should_return_the_steps_field() {
    for( keyword in Keywords.getConstructors() ) {
      if( keyword != "Custom" ) {
        var returnValue = capturedCallReturns.get(keyword);
        Assert.areSame(decoratedInstance.steps, returnValue,
          'Captured value for keyword method "${keyword.toLowerCase()}" '
          + 'should be "${decoratedInstance.steps}", but was: "${returnValue}"');
      }
    }
  }

  public function the_context_should_be_notified_about_each_keyword_call() {
    var capturedKeywords = new CaptureMatcher<Keywords>();

    var numKeywords = Keywords.getConstructors().length - 1; //excludes 'Custom' Keyword

    Mockatoo.verify(
      decoratedInstance.context.stepKeyword(capturedKeywords.matcher()), times(numKeywords));

    Assertions.arraysAreEquals(
      [Given, When, Then, And, But],
      capturedKeywords.all
    );
  }

  public function it_should_have_a_method_resetSteps() {
    checkMethod(decoratedInstance, "resetSteps");
  }

  public function the_steps_field_should_have_another_value() {
    Assert.areNotSame(capturedSteps, decoratedInstance.steps,
      "Steps field should have been resetted");
  }

  /* **************************************************************************/

  function checkMethod( obj: Dynamic, methodName: String ) : Dynamic {
    var method = Reflect.field(obj, methodName);

    Assert.isNotNull(method,
      'Field ${methodName} not found in object (${obj}) of type: ${Type.typeof(obj)}');
    Assert.isTrue(Reflect.isFunction(method), 'Field ${methodName} is not a function');

    return method;
  }

  function hasKeywordMethod(obj: Dynamic, methodName: String) {
    var clazz = Type.getClass(obj);
    Assert.isNotNull(clazz, "Failed to load class of ${obj}");

    var className = Type.getClassName(clazz);
    var fields = Type.getInstanceFields(clazz);

    Assert.isTrue(fields.indexOf(methodName) >= 0,
      'Method "${methodName}" not found in class "${className}". Current fields are: ${fields}');
  }
}

@:build(hxgiven.macros.ScenarioClassDecorator.decorateAll())
class DecoratedScenarioClass {
  public var context:Context;

  @steps
  public var steps:SampleSteps;

  public function new() {

  }

}

class SampleSteps{
  public function new() {

  }
}
