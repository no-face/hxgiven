package hxgiven.test.features;

import massive.munit.Assert;
import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;

import hxgiven.Context;


class SharedContextTest extends hxgiven.test.TestFixture<SharedContextSteps>{

  @Before
  public function setup() {
    super.resetSteps();
  }

  @Test
  public function Access_shared_context() {
    when().acessing_the_shared_context_instance();
    then().a_valid_context_should_be_returned();
  }

  @Test
  public function Set_shared_context() {
    given().an_existing_shared_context();
    given().a_new_context_instance();
    when().setting_the_shared_context_to_the_new_context();
    then().the_shared_context_value_should_change_to_the_new_context();
  }

  @Test
  public function Get_context_from_a_Contextual_subclass_instance() {
    given().a_Contextual_instance_with_a_valid_context();
    when().getting_the_context_from_that_object();
    then().the_object_context_attribute_should_be_returned();
  }

  @Test
  public function Get_context_from_an_anonymous_object() {
    given().an_anonymous_object_with_a_non_null_context_attribute();
    when().getting_the_context_from_that_object();
    then().the_object_context_attribute_should_be_returned();
  }

  @Test
  public function Get_shared_context_from_an_empty_ContextSource() {
    given().a_ContextSource_with_a_null_Context();
    when().getting_the_context_from_that_object();
    then().the_shared_context_should_be_returned();
  }
}


class SharedContextSteps extends hxgiven.BaseSteps<SharedContextSteps>{
  var capturedCtx: Context;
  var expectedCtx: Context;
  var contextSource: ContextSource;

  //when
  public function acessing_the_shared_context_instance() : Void {
    capturedCtx = Context.shared();
  }

  //then
  public function a_valid_context_should_be_returned() : Void {
    Assert.isNotNull(capturedCtx, "Context is null");
  }

  public function an_existing_shared_context( ) : Void {
    this.when().acessing_the_shared_context_instance();
    this.then().a_valid_context_should_be_returned();
  }
  public function a_new_context_instance( ) : Void {
    expectedCtx = new Context();
  }
  public function setting_the_shared_context_to_the_new_context( ) : Void {
    Context.shared(expectedCtx);
  }
  public function the_shared_context_value_should_change_to_the_new_context( ) : Void {
    Assert.areSame(Context.shared(), expectedCtx, "Shared context instance is not the expected");
  }

  /* Get context */

  public function /* given */ a_Contextual_instance_with_a_valid_context( ) {
    this.given().a_new_context_instance();

    var c:Contextual = mock(Contextual);
    c.context = expectedCtx;

    this.contextSource = c;
  }

  public function /* given */ an_anonymous_object_with_a_non_null_context_attribute(){
    this.given().a_new_context_instance();

    this.contextSource = {
      context: expectedCtx
    };
  }

  public function /* given */ a_ContextSource_with_a_null_Context() {
    this.contextSource = (null: Context);
  }

  public function /* when */ getting_the_context_from_that_object( ) {
    capturedCtx = Context.get(this.contextSource);
  }

  public function /* then */ the_object_context_attribute_should_be_returned( ) {
    Assert.areSame(expectedCtx, capturedCtx);
  }

  public function /* then */ the_shared_context_should_be_returned( ) {
    Assert.areSame(Context.shared(), capturedCtx);
  }

}
