package hxgiven.test.features;

import hxgiven.test.steps.ContextTestSteps;

class RunStepFunctionTest extends hxgiven.test.TestFixture<ContextTestSteps>{

	@Before
	public function setup() : Void {
		resetSteps();
	}

	@Test
	public function running_step_function() : Void {
		given().a_step_function();
		when().running_a_step_passing_that_function();
		then().the_function_should_be_called();
	}

	@Test
	public function running_step_function_with_args( ) : Void {
		given().a_step_function_that_accepts_some_arguments();
		when().running_that_step_passing_an_array_of_arguments([1, 2, 3]);
		then().the_function_should_receive_that_arguments();
	}

	@Test
	public function running_step_fails( ) : Void {
		given().a_step_function_that_throws_an_exception("AnyError");
		when().running_that_step();
		then().the_exception_should_have_been_rethrown();
	}
}
