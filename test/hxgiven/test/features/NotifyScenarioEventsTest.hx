package hxgiven.test.features;

import massive.munit.Assert;
import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;

import hxgiven.test.matchers.CaptureMatcher;

import hxgiven.model.*;

class NotifyScenarioEventsTest extends hxgiven.test.TestFixture<ScenarioEventsSteps>  {

  @Before
  public function setup() {
    super.resetSteps();
  }

  @Test
  public function Run_step_with_null_reporter() {
    given().a_context_with_a_null_reporter();
    when().running_a_step();
    then().no_error_should_be_thrown();
  }

  @Test
  public function Start_scenario_with_null_reporter() {
    given().a_context_with_a_null_reporter();
    when().a_scenario_is_started();
    then().no_error_should_be_thrown();
  }

  @Test
  public function Notify_scenario_start( ) {
    given().a_context_with_a_valid_reporter();
    when().a_scenario_is_started();
    then()
      .the_reporter_onScenarioStart_method_should_be_called();
      and().the_scenario_info_should_have_the_scenario_formatted_name();
  }

  @Test
  public function Notify_scenario_success(){
    given().a_context_with_a_valid_reporter();
    given().a_scenario_has_started();
    when().that_scenario_succeeds();
    then()
      .the_reporter_should_be_notified_of_the_scenario_success();
      and().the_received_ScenarioResult_should_have_the_status_of(ProgressStatus.Succeed);
      and().the_received_ScenarioResult_should_have_the_info_of_the_started_Scenario();
  }

  @Test
  public function Notify_succeeded_scenario_with_steps(){
    given()
      .a_context_with_a_valid_reporter();
    given()
      .a_scenario_has_started();
      and().some_steps_have_been_executed();

    when().that_scenario_succeeds();

    then()
      .the_reporter_should_be_notified_of_the_scenario_success();
      and().the_received_ScenarioResult_should_have_the_list_of_executed_steps();
      and().each_step_should_have_the_correspondent_keyword();
  }

  @Test
  public function Notify_failed_scenario_with_steps(){
    given()
      .a_context_with_a_valid_reporter();

    given()
      .a_scenario_has_started();
      and().some_steps_have_been_executed();
      and().a_step_has_failed();

    when()
      .the_scenario_failure_is_notified();

    then()
      .the_reporter_should_be_notified_of_the_scenario_failure();
      and().the_received_ScenarioResult_should_have_the_status_of(ProgressStatus.Failed);
      and().the_received_ScenarioResult_should_have_the_failure_cause();
      and().the_failed_step_should_be_listed_at_the_executed_steps();
  }

  @Test
  public function Notify_internal_steps() : Void {
    given().a_context_with_a_valid_reporter();

    given().a_scenario_has_started();

    when().an_step_calls_another_internal_step();

    then()
      .the_internal_step_should_not_be_directly_reported();
      but().the_reported_step_should_store_the_executed_internal_steps();
  }

  @Test
  public function Internal_scenario() : Void {
      /* Not implemented */
  }
}


class ScenarioEventsSteps extends hxgiven.BaseSteps<ScenarioEventsSteps>  {
  var ctx:Context;
  var startedScenarioInfo:ScenarioInfo;

  var capturedError: Dynamic;
  var capturedScenarioInfo  : CaptureMatcher<ScenarioInfo>;
  var capturedScenarioResult: CaptureMatcher<ScenarioResult>;

  var expectedFormattedName: String;
  var expectedSteps: Array<StepResult>;
  var expectedFailure: Failure;

  var internalStepName:String;

  public function new() {
    super();

    this.capturedScenarioInfo = new CaptureMatcher<ScenarioInfo>();
    this.capturedScenarioInfo.nonNullOnly = true;
    this.capturedScenarioResult = new CaptureMatcher<ScenarioResult>();
    this.capturedScenarioResult.nonNullOnly = true;

    this.expectedSteps = [];
    this.expectedFailure = null;

    this.internalStepName = "Internal step";
  }

  public function a_context_with_a_null_reporter(  ) {
    ctx = new Context();
  }

  public function a_context_with_a_valid_reporter( ) {
    this.given().a_context_with_a_null_reporter();
    ctx.reporter = mock(hxgiven.Reporter);
  }

  public function a_scenario_has_started() : Void {
    when().a_scenario_is_started();
  }

  public function some_steps_have_been_executed() : Void {
    runStep(Given, "step1");
    runStep(When , "step2");
    runStep(Then , "step3");
  }

  public function a_step_has_failed() {
    runStep(Then, "failed step", ProgressStatus.Failed);
  }

  public function a_scenario_is_started( ) : Void {
    try{
      this.startedScenarioInfo = new ScenarioInfo("lorem_ipsum");
      this.expectedFormattedName = "lorem ipsum";
      ctx.scenarioStarted(startedScenarioInfo);
    }
    catch(err: Dynamic){
      this.capturedError = err;
    }
  }

  public function that_scenario_succeeds( ) : Void {
    ctx.scenarioFinished();
  }

  public function the_scenario_failure_is_notified() {
    this.expectedFailure = new Failure("Error");
    ctx.scenarioFinished(expectedFailure);
  }

  public function running_a_step(  ) {
    try{
      ctx.runStep("Sample step", [], function(a: StepArgs){});
    }
    catch(err: Dynamic){
      this.capturedError = err;
    }
  }

  public function an_step_calls_another_internal_step() {
    ctx.runStep("External step", [], function( a: StepArgs ){
      ctx.runStep(internalStepName, [], function (a: StepArgs ) {});
    });
  }

  public function no_error_should_be_thrown(  ) {
    Assert.isNull(capturedError, 'Captured error: "${capturedError}"');
  }

  public function the_reporter_onScenarioStart_method_should_be_called() {
    Mockatoo.verify(ctx.reporter.onScenarioStart(capturedScenarioInfo.matcher()));
  }

  public
  function the_reporter_should_be_notified_of_the_scenario_success() {
    Mockatoo.verify(ctx.reporter.onScenarioSuccess(capturedScenarioResult.matcher()));
  }

  public
  function the_reporter_should_be_notified_of_the_scenario_failure() {
    Mockatoo.verify(ctx.reporter.onScenarioFailure(capturedScenarioResult.matcher()));
  }

  public function the_scenario_info_should_have_the_scenario_formatted_name() {
    Assert.areEqual(expectedFormattedName, capturedScenarioInfo.captured.formattedName);
  }

  public function the_received_ScenarioResult_should_have_the_info_of_the_started_Scenario() {
    Assert.areEqual(startedScenarioInfo, capturedScenarioResult.captured.scenarioInfo);
  }

  public function the_received_ScenarioResult_should_have_the_status_of( expectedStatus: ProgressStatus ) {
    Assert.areEqual(expectedStatus, capturedScenarioResult.captured.status);
  }

  public function the_received_ScenarioResult_should_have_the_list_of_executed_steps() {
    Assertions.arraysAreEquals(this.expectedSteps, capturedScenarioResult.captured.executedSteps,
      function ( expected:StepResult, actual:StepResult  ) : Bool {
        if( expected == null || actual == null ) {
          return expected == actual;
        }

        return expected.status == actual.status
            && expected.stepInfo.name == actual.stepInfo.name
            && expected.stepInfo.args.args == expected.stepInfo.args.args;
      });
  }

  public function each_step_should_have_the_correspondent_keyword() {
    var steps = capturedScenarioResult.captured.executedSteps;

    for( i in 0...steps.length ) {
      var k = steps[i].stepInfo.keyword;
      var expected = expectedSteps[i].stepInfo.keyword;
      Assert.areEqual(expected, k,
        'Keyword "${k}" of step ${i} is not the expected (${expected})');
    }

  }

  public function the_failed_step_should_be_listed_at_the_executed_steps(){
    this.then().the_received_ScenarioResult_should_have_the_list_of_executed_steps();
  }

  public function the_received_ScenarioResult_should_have_the_failure_cause() {
    Assert.isNotNull(capturedScenarioResult.captured.failure,
                      "ScenarioResult 'failure' should not be null!");

    Assert.areEqual(expectedFailure, capturedScenarioResult.captured.failure);
  }

  public function the_internal_step_should_not_be_directly_reported() {
    var isInternalStep = matchesStep(this.internalStepName);
    var hasStepInfo = hasStepInfoMatcher(this.internalStepName);

    Mockatoo.verify(ctx.reporter.onStepStart(isInternalStep), never);
    Mockatoo.verify(ctx.reporter.onStepSuccess(hasStepInfo), never);
    Mockatoo.verify(ctx.reporter.onStepFailure(hasStepInfo), never);
  }

  public function the_reported_step_should_store_the_executed_internal_steps() {
    var externalStep = new CaptureMatcher<StepResult>();

    Mockatoo.verify(ctx.reporter.onStepSuccess(externalStep.matcher()), 1);

    var stepResult = externalStep.captured;

    Assert.isNotNull(stepResult);
    Assert.isTrue(stepResult.internalSteps.length > 0,
      "Captured step has no internal steps");
  }

  /* *************************** Helpers **************************************/

  function runStep(keyword: Keywords, stepName: String, ?status:ProgressStatus) {
    status = if(status != null) status else ProgressStatus.Succeed;

    var args:Array<Dynamic> = [];
    var expectedStepInfo = new StepInfo(keyword, stepName, new StepArgs(args));
    expectedSteps.push(new StepProgress(expectedStepInfo, status));

    ctx.stepKeyword(keyword);

    try{
      ctx.runStep(stepName, args, function ( a: StepArgs ){
        if( status == ProgressStatus.Failed ) {
          throw "Step failed!";
        }
      });
    }
    catch(err: Dynamic){
      Assert.areEqual(ProgressStatus.Failed, status,
        'The step "${stepName}" should not fail, but exception was thrown: "${err}"');
    }

  }

  function matchesStep( stepName: String ) : Matcher {
    return customMatcher(this.isStepFunction(stepName));
  }
  function hasStepInfoMatcher( stepName: String ) : Matcher {
    var stepInfoMatches = isStepFunction(stepName);

    return customMatcher(function ( value: Dynamic ) {
      return stepInfoMatches(value.stepInfo);
    });
  }
  function isStepFunction( stepName: String ) {
    return function( value: Dynamic ) {
      if( !Std.is(value, StepInfo) || value == null) {
        return false;
      }
      var stepInfo = (value : StepInfo);

      return stepInfo.name == stepName;
    }
  }
}
