package hxgiven.test.features;

import massive.munit.Assert;

import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;

import hxgiven.test.steps.ContextTestSteps;

import hxgiven.report.NamesFormatter;

class ReportStepEventsTest extends hxgiven.test.TestFixture<ReportStepEventsSteps>{

	@Before
	public function setup() : Void {
		resetSteps();
	}

	@Test
	public function report_step_start( ) : Void {
		given().the_context_has_a_valid_reporter();
		when().running_some_step();
		then().the_reporter_should_be_notified_about_the_step_start();
	}

	@Test
	public function report_step_success( ) : Void {
		given().the_context_has_a_valid_reporter();
		when().running_a_step_that_succeeds();
		then().the_reporter_should_be_notified_about_the_step_success();
	}

	@Test
	public function report_step_failure( ) : Void {
		given().the_context_has_a_valid_reporter();
		given().a_step_function_that_throws_an_exception("AnyError");
		when().running_that_step();
		then().the_failure_should_have_been_reported();
  }

  @Test
  public function formatted_step_name() {
    given().the_context_has_a_valid_reporter();
    when().running_some_step();
    then().the_received_StepInfo_should_have_the_formatted_step_name();
  }
}


class ReportStepEventsSteps extends ContextTestSteps {

  public function new() {
    super();
  }

	public function the_context_has_a_valid_reporter() {
		ctx.reporter = mock(Reporter);
	}


	public function the_reporter_should_be_notified_about_the_step_start() {
		Mockatoo.verify(ctx.reporter.onStepStart(captureStepInfo.matcher()));

		checkCapturedStepInfo();
	}

	public function the_reporter_should_be_notified_about_the_step_success() {
		Mockatoo.verify(ctx.reporter.onStepSuccess(captureStepResult.matcher()));

		checkCapturedStepResult();
	}

	public function the_failure_should_have_been_reported() {
		Mockatoo.verify(
			ctx.reporter.onStepFailure(captureStepResult.matcher()));

		checkCapturedStepResult(expectedError);
	}

  public function the_received_StepInfo_should_have_the_formatted_step_name() {
    the_reporter_should_be_notified_about_the_step_start();
    the_reporter_should_be_notified_about_the_step_success();

    var expectedFormattedName = new NamesFormatter().format(this.lastStepName);
    var capturedFormattedName = captureStepInfo.captured.formattedName;

    Assert.areEqual(expectedFormattedName, capturedFormattedName);
  }
}
