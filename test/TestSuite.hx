import massive.munit.TestSuite;

import hxgiven.test.samples.BasicTest;
import hxgiven.test.tests.macros.ScenariosDecoratorTest;
import hxgiven.test.tests.macros.StepsDecoratorTest;
import hxgiven.test.tests.macros.AutoBuilderDecoratorTest;
import hxgiven.test.tests.macros.ScenarioClassDecoratorTest;
import hxgiven.test.tests.macros.AutoDecoratedStepsTest;
import hxgiven.test.tests.ResultPrinterTest;
import hxgiven.test.tests.BaseScenarioTest;
import hxgiven.test.tests.NamesFormatterTest;
import hxgiven.test.tests.BaseStepsTest;
import hxgiven.test.features.NotifyScenarioEventsTest;
import hxgiven.test.features.ReportStepEventsTest;
import hxgiven.test.features.RunStepFunctionTest;
import hxgiven.test.features.SharedContextTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(hxgiven.test.samples.BasicTest);
		add(hxgiven.test.tests.macros.ScenariosDecoratorTest);
		add(hxgiven.test.tests.macros.StepsDecoratorTest);
		add(hxgiven.test.tests.macros.AutoBuilderDecoratorTest);
		add(hxgiven.test.tests.macros.ScenarioClassDecoratorTest);
		add(hxgiven.test.tests.macros.AutoDecoratedStepsTest);
		add(hxgiven.test.tests.ResultPrinterTest);
		add(hxgiven.test.tests.BaseScenarioTest);
		add(hxgiven.test.tests.NamesFormatterTest);
		add(hxgiven.test.tests.BaseStepsTest);
		add(hxgiven.test.features.NotifyScenarioEventsTest);
		add(hxgiven.test.features.ReportStepEventsTest);
		add(hxgiven.test.features.RunStepFunctionTest);
		add(hxgiven.test.features.SharedContextTest);
	}
}
