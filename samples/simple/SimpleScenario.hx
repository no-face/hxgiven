import hxgiven.Scenario;
import hxgiven.Steps;

class SimpleScenario implements Scenario{

  static function main() {
    hxgiven.Defaults.reportLevel = hxgiven.report.ReportLevel.Full;

    var s = new SimpleScenario();

    s.simple_scenario();

    try{
      s.failed_scenario();
    }
    catch(e:Dynamic) {}

  }

  public function new() {}

  @steps
  var steps:SimpleSteps;

  @scenario
  public function simple_scenario() {
    given().an_initial_state();
    when().an_action_occurs();
    then().a_post_condition_should_be_valid();
  }

  @scenario
  public function failed_scenario() {
    given().an_initial_state();
    when().an_error_occurs();
    then().this_step_should_not_be_called();
  }
}

class SimpleSteps extends Steps<SimpleSteps>{

  public function new() {
    super();
  }

  @step
  public function an_initial_state() {}

  @step
  public function an_action_occurs() {}

  @step
  public function an_error_occurs() {
    throw "An error ocurred!";
  }

  @step
  public function a_post_condition_should_be_valid() {}

  @step
  public function this_step_should_not_be_called() {}
}
