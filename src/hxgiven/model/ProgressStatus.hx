package hxgiven.model;

enum ProgressStatus {
  NotExecuted;
  Succeed;
  Failed;
}
