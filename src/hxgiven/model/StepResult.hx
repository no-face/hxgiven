package hxgiven.model;

interface StepResult {
  public var status       (default, null): ProgressStatus;
  public var stepInfo     (default, null): StepInfo;
  public var failure      (default, null): Failure;
  public var internalSteps(default, null): Array<StepResult>;
}
