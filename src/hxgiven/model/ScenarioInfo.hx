package hxgiven.model;

class ScenarioInfo {

  public var name(default, default):String;
  public var formattedName(default, default):String;

  public function new(?name: String, ?formattedName: String) {
    this.name = name;
    this.formattedName = formattedName;
  }

}
