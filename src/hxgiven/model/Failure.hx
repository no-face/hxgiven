package hxgiven.model;


class Failure {
  public var error:Dynamic;

  public function new(error: Dynamic) {
    this.error = error;
  }

  public function rethrow() : Void {
    throw error;
  }
}
