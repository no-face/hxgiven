package hxgiven.model;

class ScenarioProgress implements ScenarioResult {

  public var scenarioInfo(default, default): ScenarioInfo;
  public var status(default, default): ProgressStatus;
  public var failure(default, default): Null<Failure>;
  public var executedSteps(default, null): Array<StepResult>;

  public function new(?scenarioInfo: ScenarioInfo) {
    this.scenarioInfo = scenarioInfo;
    this.executedSteps = [];
    this.status = ProgressStatus.NotExecuted;
  }

  public function finished( ?failure: Failure ) : Void {
    this.status = statusIfFailed(failure);
    this.failure = failure;
  }

  public function addStep(stepResult: StepResult) : Void {
    this.executedSteps.push(stepResult);
  }

  function statusIfFailed(?failure: Failure ) : ProgressStatus {
    if(failure == null)
      return ProgressStatus.Succeed;
    else
      return ProgressStatus.Failed;
  }
}
