package hxgiven.model;


class StepProgress implements StepResult {

  public var status       (default, null): ProgressStatus;
  public var stepInfo     (default, null): StepInfo;
  public var failure      (default, null): Failure;
  public var internalSteps(default, null): Array<StepResult>;

  public function new(
      stepInfo      : StepInfo,
      ?status       : ProgressStatus,
      ?failure      : Failure,
      ?internalSteps: Array<StepResult>)
  {
    this.stepInfo = stepInfo;
    this.status = if(status != null) status else ProgressStatus.NotExecuted;
    this.failure = failure;
    this.internalSteps = if(internalSteps == null) [] else internalSteps;
  }

  public function addInternalStep( step: StepResult ) : Void {
    this.internalSteps.push(step);
  }

  public function finish( ?failure: Failure ) : Void {
    //To-do: throw if status is already finished (Succeed or Failed)

    if( failure == null ) {
      this.status = ProgressStatus.Succeed;
    }
    else{
      this.status = ProgressStatus.Failed;
      this.failure = failure;
    }
  }
}
