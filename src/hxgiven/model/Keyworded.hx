package hxgiven.model;

interface Keyworded<T>{

  public function given() : T;
  public function when()  : T;
  public function then()  : T;
  public function and()   : T;
  public function but()   : T;
}
