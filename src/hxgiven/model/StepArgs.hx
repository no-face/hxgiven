package hxgiven.model;

typedef ArgsList = Array<Dynamic>;

class StepArgs {
	public var args: ArgsList;

	public function new(args: ArgsList) {
		this.args = args;
	}

}
