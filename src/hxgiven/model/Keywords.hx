package hxgiven.model;

enum Keywords {
  Given;
  When;
  Then;
  And;
  But;
  Custom(keyword: String);
}
