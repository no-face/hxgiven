package hxgiven.model;

class StepInfo {

  public var name: String;
  public var args: StepArgs;
  public var keyword: Null<Keywords>;
  public var formattedName: String;

  public function new(keyword: Null<Keywords>, name:String, args: StepArgs, ?formattedName: String) {
    this.keyword = keyword;
    this.name = name;
    this.args = args;
    this.formattedName = formattedName;
  }
}
