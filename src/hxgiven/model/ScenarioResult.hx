package hxgiven.model;


interface ScenarioResult {
  public var scenarioInfo(default, null): ScenarioInfo;
  public var status(default, null): ProgressStatus;
  public var executedSteps(default, null): Array<StepResult>;
  public var failure(default, null): Null<Failure>;
}
