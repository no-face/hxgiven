package hxgiven;

import hxgiven.model.*;

class AbstractKeyworded<T> implements Keyworded<T> implements Contextual{

  public var context(default, default):Context;

	function new(){} //avoid creation

	public function getInstance() : T { //To-Do: throw "not implemented"
		return null;
	}

	public function given() : T { return keyword(Given);}
	public function when()  : T { return keyword(When);}
	public function then()  : T { return keyword(Then);}
	public function and()   : T { return keyword(And);}
	public function but()   : T { return keyword(But);}

  function keyword(k: Keywords) : T {
    Context.get(this).stepKeyword(k);

    return getInstance();
  }
}
