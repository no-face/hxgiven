package hxgiven;

import haxe.Constraints;

@:generic
@:autoBuild(hxgiven.macros.ScenarioClassDecorator.decorateAll())
interface Scenario extends hxgiven.macros.AutoDecorateScenario
{
}
