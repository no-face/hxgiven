package hxgiven;

import hxgiven.Context;
import hxgiven.model.Keyworded;
import hxgiven.model.Keywords;

class BaseSteps<T> extends AbstractKeyworded<T>{

	public function new(){
    super();
  }

	override function getInstance() : T {
		return cast this;
	}
}
