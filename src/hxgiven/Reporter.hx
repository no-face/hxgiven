package hxgiven;

import hxgiven.model.ScenarioInfo;
import hxgiven.model.Failure;
import hxgiven.model.StepInfo;
import hxgiven.model.StepResult;
import hxgiven.model.ScenarioResult;

interface Reporter {
  function onScenarioStart( scenarioInfo: ScenarioInfo ) : Void;
  function onScenarioSuccess( scenarioResult: ScenarioResult ) : Void;
  function onScenarioFailure( scenarioResult: ScenarioResult ) : Void;

  function onStepStart  ( stepInfo: StepInfo ) : Void;
  function onStepSuccess( stepInfo: StepResult ) : Void;
  function onStepFailure( stepInfo: StepResult ) : Void;
}
