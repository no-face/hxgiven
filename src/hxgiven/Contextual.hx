package hxgiven;

interface Contextual {
    public var context(default, null):Context;
}
