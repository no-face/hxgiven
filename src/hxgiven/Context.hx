package hxgiven;

import hxgiven.model.ScenarioInfo;
import hxgiven.model.ScenarioResult;
import hxgiven.model.ScenarioProgress;
import hxgiven.model.Failure;
import hxgiven.model.Keywords;
import hxgiven.model.ProgressStatus;
import hxgiven.model.StepInfo;
import hxgiven.model.StepArgs;
import hxgiven.model.StepProgress;

import hxgiven.report.NamesFormatter;

typedef ContextualConstraint = {
	public var context(default, null):Context;
}

abstract ContextSource(Context) from Context to Context {

	public inline function new( c : Context ) {
		this = c;
	}

	@:from
  static public function fromContextual<T:ContextualConstraint>(c:T)	 {
    return new ContextSource(c.context);
  }

	@:from
  static public function fromDynamic(d:Dynamic) {
    return new ContextSource(null);
  }
}

class Context{
	static var sharedContext:Context;

	public static function shared(?ctx: Context) {
			if( ctx != null ) {
				sharedContext = ctx;
			}
			else if( sharedContext == null ) {
				sharedContext = new Context();
				sharedContext.reporter = Defaults.buildReporter();
			}

			return sharedContext;
	}

	public static function get(ctxSource: ContextSource) : Context {
		var ctx:Context = ctxSource;
		if( ctx == null ) {
			return shared();
		}

		return ctx;
	}

	public var reporter:Reporter;

	var namesFormatter:NamesFormatter;

	var currentScenario: ScenarioProgress;
	var stepsStack		 : Array<StepProgress>;
	var lastKeyword		 : Null<Keywords>;

	public function new()
	{
		namesFormatter = new NamesFormatter();
		stepsStack = [];
	}

	public function scenarioStarted( scenarioInfo: ScenarioInfo ) : Void {
		//TO-DO: check if already there is a currentScenario
		//TO-DO: check if scenarioInfo is not null

		if( scenarioInfo.formattedName == null ) {
			scenarioInfo.formattedName = formatName(scenarioInfo.name);
		}

		this.currentScenario = new ScenarioProgress(scenarioInfo);

		if( reporter != null ) {
			reporter.onScenarioStart(scenarioInfo);
		}
	}

	public function scenarioFinished(?failure: Failure) : Void {
		if( currentScenario != null ) { //should throw if scenario is null
			this.currentScenario.finished(failure);
		}

		if( reporter != null ) {
			if( failure == null ) {
				reporter.onScenarioSuccess(this.currentScenario);
			}
			else {
				reporter.onScenarioFailure(this.currentScenario);
			}
		}

		this.currentScenario = null;
	}

	public function stepKeyword(keyword: Keywords) : Void {
		this.lastKeyword = keyword;
	}

	function consumeKeyword() : Null<Keywords> {
		var k = lastKeyword;
		lastKeyword = null;

		return k;
	}

	public function runStep( stepName: String, args: ArgsList, step: StepFunction ) : Void {
		var stepArgs = new StepArgs(args);
		var stepInfo = new StepInfo(consumeKeyword(), stepName, stepArgs, formatName(stepName));

		startStep(stepInfo);

		try {
			step(stepArgs);
			finishStep();
		}
		catch(e:Dynamic) {
			finishStep(new Failure(e));

			throw e;
		}
	}

	function formatName(stepName: String) : String {
		return namesFormatter.format(stepName);
	}

	function startStep( stepInfo: StepInfo ) {
		stepsStack.push(new StepProgress(stepInfo));

		var isInternalStep = stepsStack.length > 1;

		if( reporter != null && !isInternalStep) {
			reporter.onStepStart(stepInfo);
		}
	}

	function finishStep(?f: Failure) {
		var step = stepsStack.pop();

		// if( step == null ) {
		// 	throw ...
		// }
		step.finish(f);

		if(stepsStack.length > 0){//step is internal
			var parent = stepsStack[stepsStack.length - 1];
			parent.addInternalStep(step);

			return; //does not notify
		}

		if( currentScenario != null ) {
			currentScenario.addStep(step);
		}
		if( reporter != null ) {
			if( f == null ) {
				reporter.onStepSuccess(step);
			}
			else{
				reporter.onStepFailure(step);
			}
		}
	}
}

typedef StepFunction = StepArgs -> Void;
