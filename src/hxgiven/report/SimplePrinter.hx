package hxgiven.report;

import hxgiven.model.Failure;

class SimplePrinter implements Printer{
    public function new()
    {}

    public function warning( text: String ) : Void{
      print(text);
    }
    public function success( text: String ) : Void{
      print(text);
    }
    public function info( text: String ) : Void{
      print(text);
    }
    public function error( text: String ) : Void{
      print(text);
    }
    public function failure( f: Failure ) : Void{
      delimiter();
      print(Std.string(f.error));
      delimiter();
    }

    public function line() : Void {
      print("");
    }

    function delimiter(){
      var buf = new StringBuf();

      for( i in 0...50 ) {
        buf.add('-');
      }

      print(buf.toString());
    }

    function print( text: String ) : Void {
      Sys.println(text);
    }
}
