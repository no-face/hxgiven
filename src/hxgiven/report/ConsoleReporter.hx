package hxgiven.report;

import hxgiven.model.ScenarioResult;

class ConsoleReporter extends BaseReporter {
  var resultPrinter:ResultPrinter;

  public function new() {
    super();
    resultPrinter = new ResultPrinter(Defaults.buildPrinter());
  }

  override public function onScenarioSuccess( scenarioResult: ScenarioResult ) : Void{
    resultPrinter.printResult(scenarioResult);
  }
  override public function onScenarioFailure( scenarioResult: ScenarioResult ) : Void{
    resultPrinter.printResult(scenarioResult);
  }
}
