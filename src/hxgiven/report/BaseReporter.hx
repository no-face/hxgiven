package hxgiven.report;

import hxgiven.model.*;

class BaseReporter implements hxgiven.Reporter
{
    public function new()
    {}

    public function onScenarioStart( scenarioInfo: ScenarioInfo ) : Void{}
    public function onScenarioSuccess( scenarioResult: ScenarioResult ) : Void{}
    public function onScenarioFailure( scenarioResult: ScenarioResult ) : Void{}

    public function onStepStart  ( stepInfo: StepInfo ) : Void{}
    public function onStepSuccess( stepInfo: StepResult ) : Void{}
    public function onStepFailure( stepInfo: StepResult ) : Void{}
}
