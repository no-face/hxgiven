package hxgiven.report;

using haxe.EnumTools;
using haxe.EnumTools.EnumValueTools;

import hxgiven.model.*;
import hxgiven.report.ReportLevel;

class ResultPrinter{
  public var printer(default, default): Printer;
  public var level(default, default): ReportLevel;
  public var tabChar(default, default): String = '  ';

  public function new(?printer: Printer){
    this.printer = printer;
    this.level = hxgiven.Defaults.reportLevel;
  }

  public function printResult(result: ScenarioResult) : Void {
    var failed = (result.status == ProgressStatus.Failed);

    if( levelIsAtLeast(Summary) || (failed && levelIsAtLeast(Minimum))) {
      printScenarioTitle(result);
    }
    if( levelIsAtLeast(Detailed) || (failed && levelIsAtLeast(Minimum))) {
      printSteps(result);

      if( result.failure != null) {
        printer.failure(result.failure);
      }
    }

    if( levelIsAtLeast(Detailed) || (failed && levelIsAtLeast(Minimum))) {
      printer.line();
    }
  }

  function levelIsAtLeast( minimumLevel: ReportLevel ) : Bool {
    return this.level.getIndex() >= minimumLevel.getIndex();
  }

  function printScenarioTitle( result: ScenarioResult ) : Void {
    var title = 'Scenario: ${result.scenarioInfo.formattedName}';

    if( result.status == ProgressStatus.Succeed ) {
      printer.success(title);
    }
    else if(result.status == ProgressStatus.Failed) {
      printer.error(title);
    }
  }

  function printSteps( scenario: ScenarioResult, ?steps: Array<StepResult>, level:Int=1 ) : Void {
    if( steps == null ) {
      steps = scenario.executedSteps;
    }

    for( s in steps ) {
      printStep(scenario, s, level);
    }
  }

  function printStep( scenario: ScenarioResult, s: StepResult, level:Int) : Void {
    var line = stepToString(s.stepInfo, level);

    if( s.status == ProgressStatus.Failed ) {
      printer.error(line);
      if( scenario.failure == null && s.failure != null ) {
        printer.failure(s.failure);
      }
    }
    else {
      if( scenario.status == ProgressStatus.Succeed ) {
        printer.success(line);
      }
      else{
        printer.info(line);
      }
    }

    if(levelIsAtLeast(Full) &&  s.internalSteps.length > 0 ) {
      printSteps(scenario, s.internalSteps, level+1);
    }
  }

  function stepToString( s: StepInfo, level: Int ) : String {
    var tabs = identation(level);
    var key = keywordToString(s.keyword);

    return '${tabs}${key} ${s.formattedName}';
  }

  function identation(level: Int) : String {
    var out = new StringBuf();

    for( i in 0...level ) {
      out.add(tabChar);
    }

    return out.toString();
  }

  function keywordToString( k: Null<Keywords> ) : String {
    if( k == null ) {
      return "";
    }

    switch (k) {
      case Custom(str): return str;
      default: return Std.string(k).toLowerCase();
    }
  }
}
