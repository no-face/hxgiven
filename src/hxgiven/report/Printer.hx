package hxgiven.report;

import hxgiven.model.Failure;

interface Printer{
  function warning( text: String ) : Void;
  function success( text: String ) : Void;
  function info( text: String ) : Void;
  function error( text: String ) : Void;
  function failure( f: Failure ) : Void;

  /* Print empty line */
  function line() : Void;
}
