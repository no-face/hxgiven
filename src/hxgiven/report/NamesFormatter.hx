package hxgiven.report;

using StringTools;

class NamesFormatter{
    public function new()
    {}

    public function format( name: String ) : String {
      var snakeCaseName = ~/(_)*([A-Za-z0-9]+(_)+)+([A-Za-z0-9])+/;
      var camelCaseWord = ~/([A-Z]([a-z0-9])*)/g;

      if( snakeCaseName.match(name) ) {
        return name.replace("_", " ").trim();
      }

      return camelCaseWord.map(name, function ( r: EReg ) : String {
        if( r.matchedPos().pos == 0) { //initial word
          return r.matched(0);
        }
        return " " + r.matched(0).toLowerCase();
      });
    }
}
