package hxgiven.report;

enum ReportLevel {
  Quiet;
  Minimum;
  Summary;
  Detailed;
  Full;
}
