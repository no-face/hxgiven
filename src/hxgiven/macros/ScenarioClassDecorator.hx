package hxgiven.macros;

import haxe.macro.Context;
import haxe.macro.Expr;

using haxe.EnumTools;

import hxgiven.model.Keywords;

class ScenarioClassDecorator extends BaseDecorator{
  public static var STEPS_METADATA = "steps";
  public static var RESET_STEPS_METHOD = "resetSteps";

  macro public static function decorateAll() : Array<Field> {
    return new ScenarioClassDecorator().decorate();
  }


  override function shouldDecorate( field: Field, ?fieldName: String ) : Bool {
    return super.shouldDecorate(field, fieldName)
           && hasMetadata(STEPS_METADATA, field.meta)
           && VarInfo.from(field) != null;
  }

  override public function decorateField( field: Field ) : Array<Field> {
    var autobuilder = new AutoBuilderDecorator();

    var fields = autobuilder.decorateField(field);

    for( keyword in Keywords.getConstructors() ) {
      if( keyword != "Custom" ) {
        fields.push(createKeywordFunction(field, keyword));
      }
    }

    fields.push(makeResetStepsFunction(field));

    return fields;
  }

  function createKeywordFunction( field: Field, keywordName: String ) : Field {
    var varInfo = VarInfo.from(field);

    var keyword = Keywords.createByName(keywordName);

    var func:Function = {
      expr: macro {
        hxgiven.Context.get(this).stepKeyword($v{keyword});

        return $i{field.name};
      },
      ret: varInfo.varType,
      args:[]
    };

    return {
      name: keywordName.toLowerCase(),
      access: [Access.APublic],
      kind: FieldType.FFun(func),
      pos: Context.currentPos(),
    };
  }

  function makeResetStepsFunction( field: Field ): Field {
    var fieldName = field.name;

    return {
      name: RESET_STEPS_METHOD,
      access: [Access.APublic],
      pos: Context.currentPos(),
      kind: FieldType.FFun({
        args: [],
        ret: (macro : Void),
        expr: macro {
          this.$fieldName = null;
        }
      }),
    };
  }
}
