package hxgiven.macros;

import haxe.macro.Context;
import haxe.macro.Expr;

class VarInfo{

  public static function from( field: Field ) : VarInfo {
      switch (field.kind) {
      case FVar(t, e):
        return new VarInfo(t, e);
      default:
        return null;
      }
  }

  public var varType: Null<ComplexType>;
  public var initExpr: Expr;

  public function new(varType: Null<ComplexType>, initExpr: Expr){
    this.varType = varType;
    this.initExpr = initExpr;
  }
}
