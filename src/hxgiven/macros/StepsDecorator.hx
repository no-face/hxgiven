package hxgiven.macros;

import haxe.macro.Context;
import haxe.macro.Expr;

using haxe.macro.TypeTools;

import hxgiven.model.StepArgs;

#if macro

class StepsDecorator extends FunctionDecorator{

  macro static public function decorateMethod( method: String ) : Array<Field> {
    var decorator = new StepsDecorator();

    return decorator.decorate(method);
  }

  macro static public function decorateAll() : Array<Field> {
    var decorator = new StepsDecorator();

    return decorator.decorate();
  }

  override function shouldDecorate( field: Field, ?fieldName: String) : Bool {
    return super.shouldDecorate(field, fieldName)
           && (fieldName ==field.name || hasMetadata("step", field.meta));
  }

  override function bodyExpr( field: Field, fn: haxe.macro.Function ) : Expr {
    var fname = field.name;
    var argsToArray = [for (arg in fn.args) macro $i{arg.name}];

    var vars:Array<Var> = [for(idx in 0...fn.args.length) {
      var arg = fn.args[idx];

      {
        expr: macro{stepArgs.args[$v{idx}];},
        name: arg.name,
        type: arg.type
      };
    }];

    var varDecl:Expr = {
      expr: ExprDef.EVars(vars),
      pos: Context.currentPos()
    };
    var functionBody = fn.expr;

    return macro{
      var values:Array<Dynamic> = $a{argsToArray};

      var ctx = hxgiven.Context.get(this);

      ctx.runStep($v{fname}, values, function(stepArgs:hxgiven.model.StepArgs) {
        $e{varDecl};
        $e{functionBody};
      });
    };
  }
}

#end
