package hxgiven.macros;

import haxe.macro.Context;
import haxe.macro.Expr;

import hxgiven.model.ScenarioInfo;
import hxgiven.model.ProgressStatus;
import hxgiven.model.Failure;

#if macro

class ScenariosDecorator extends FunctionDecorator{

	macro public static function decorateMethod( scenarioMethod: String ) : Array<Field> {

		return new ScenariosDecorator().decorate(scenarioMethod);
	}

	macro public static function decorateAll() : Array<Field> {
		return new ScenariosDecorator().decorate();
	}

	public function new(){
		super();
	}

	override function shouldDecorate( field: Field, ?fieldName: String) : Bool {
		return super.shouldDecorate(field, fieldName)
					 && (fieldName == field.name || hasMetadata("scenario", field.meta));
	}

  override function bodyExpr( field: Field, fn: haxe.macro.Function ) : Expr {
    return macro{
			var scenarioInfo = new hxgiven.model.ScenarioInfo($v{field.name});
			hxgiven.Context.get(this).scenarioStarted(scenarioInfo);

			try{
				var fn = function(){
					$e{fn.expr}
				};
				fn();
			}
			catch(e:Dynamic) {
				hxgiven.Context.get(this).scenarioFinished(new hxgiven.model.Failure(e));

				throw e;
			}
		};
  }

  override function afterExpr( field: Field, fn: haxe.macro.Function ) : Expr {
    return macro{
			hxgiven.Context.get(this).scenarioFinished();
		};
  }
}

#end
