package hxgiven.macros;

import haxe.macro.Context;
import haxe.macro.Expr;

class FunctionDecorator {

  var fields: Array<Field>;

  public function new() {
    this.fields = Context.getBuildFields();
  }

  public function decorate( ?fieldName: String ): Array<Field> {
    for( field in this.fields) {
      if(shouldDecorate(field, fieldName)){
        decorateField(field);
      }
    }

    return this.fields;
  }

  function shouldDecorate( field: Field, ?fieldName: String) : Bool {
    if( field == null || field.meta == null || (fieldName != null && fieldName != field.name)) {
      return false;
    }

    if( fieldName == null && field.name == "new") {
      return false; //ignore constructor by default
    }

    return isFunction(field);
  }

  function isFunction( field: Field ) : Bool {
    switch field.kind {
    case FieldType.FFun(_): return true;
    default               : return false;
    }
  }

  function decorateField( field: Field )
  {
    switch (field.kind)
			{
				case FieldType.FFun(fn):
					decorateFieldFunction(field, fn);
				default:
			}
  }

  function decorateFieldFunction( field: Field, fn: haxe.macro.Function) : Void {

    var exprs:Array<Expr> = [];

    var before = beforeExpr(field, fn);
    var after = afterExpr(field, fn);

    if(before != null){
      exprs.push(before);
    }
    exprs.push(bodyExpr(field, fn));
    if( after != null ) {
      exprs.push(after);
    }

    fn.expr = macro {$a{exprs}};
  }

  function beforeExpr( field: Field, fn: haxe.macro.Function ) : Expr {
    return null;
  }

  function bodyExpr( field: Field, fn: haxe.macro.Function ) : Expr {
    return fn.expr;
  }

  function afterExpr( field: Field, fn: haxe.macro.Function ) : Expr {
    return null;
  }

  function hasMetadata( name: String, metadata: Metadata ) : Bool {
    return null != findWhere(metadata, function (m : MetadataEntry ) {
      return m.name == name;
    });
  }

  function findField(fieldName: String) : Field {
    return findWhere(this.fields, function ( f: Field ) {
      return f.name == fieldName;
    });
  }

  function findWhere<T>( values: Iterable<T>, cond: T->Bool) : T {
    for( v in values ) {
      if( cond(v) ) {
        return v;
      }
    }

    return null;
  }
}
