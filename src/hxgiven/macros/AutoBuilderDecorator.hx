package hxgiven.macros;

import haxe.macro.Context;
import haxe.macro.Expr;

class AutoBuilderDecorator extends BaseDecorator
{
  macro public static function decorateAll() : Array<Field> {
    return new AutoBuilderDecorator().decorate();
  }

  override function shouldDecorate( field: Field, ?fieldName: String ) : Bool {
    return super.shouldDecorate(field, fieldName)
           && hasMetadata("autobuild", field.meta)
           && VarInfo.from(field) != null;
  }

  override public function decorateField( field: Field ) : Array<Field> {
    var varInfo = VarInfo.from(field);
    var varName = '_${field.name}';

    var propField = {
      name: field.name,
      doc: field.doc,
      meta: field.meta,
      access: field.access,
      kind: FieldType.FProp("get", "set", varInfo.varType, varInfo.initExpr),
      pos: field.pos
    };

    var propVar = {
      name: varName,
      doc: field.doc,
      meta: field.meta,
      access: field.access,
      kind: field.kind,
      pos: Context.currentPos()
    };

    var n = makeNewExpr(varInfo);

    var getterFunc:Function = {
      expr: macro {
        if( $i{varName} == null) {
          $i{varName} = ${n};
        }
        return $i{varName};
      },
      ret: varInfo.varType,
      args:[]
    };

    var setterFunc:Function = {
      expr: macro {
        $i{varName} = value;

        return $i{varName};
      },
      ret: varInfo.varType,
      args:[{
        name:"value",
        type:varInfo.varType
      }]
    };

    var getterField:Field = {
      name: "get_" + field.name,
      access: field.access,
      kind: FieldType.FFun(getterFunc),
      pos: Context.currentPos(),
    };

    var setterField:Field = {
      name: "set_" + field.name,
      access: field.access,
      kind: FieldType.FFun(setterFunc),
      pos: Context.currentPos(),
    };

    return [propField, propVar, getterField, setterField];
  }

  function makeNewExpr( varInfo: VarInfo ) : Expr {
    switch (varInfo.varType) {
    case TPath(tPath):
      return {
        pos: Context.currentPos(),
        expr: ENew(tPath, [])
      }
    default:
      return macro null;
    }
  }
}
