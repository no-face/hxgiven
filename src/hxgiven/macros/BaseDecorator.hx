package hxgiven.macros;

import haxe.macro.Context;
import haxe.macro.Expr;

class BaseDecorator {

  var fields: Array<Field>;

  public function new() {
    this.fields = Context.getBuildFields();
  }

  public function decorate( ?fieldName: String ): Array<Field> {
    var resultFields:Array<Field> = [];

    for( field in this.fields) {
      if(shouldDecorate(field, fieldName)){
        for( f in decorateField(field) ) {
          resultFields.push(f);
        }
      }
      else {
        resultFields.push(field);
      }
    }

    this.fields = resultFields;
    return this.fields;
  }

  function shouldDecorate( field: Field, ?fieldName: String) : Bool {
    return ( field != null
          && field.meta != null
          && (fieldName == null || fieldName == field.name));
  }

  function decorateField( field: Field ) : Array<Field>
  {
    return [field];
  }

  function hasMetadata( name: String, metadata: Metadata ) : Bool {
    return null != findWhere(metadata, function (m : MetadataEntry ) {
      return m.name == name;
    });
  }

  function findField(fieldName: String) : Field {
    return findWhere(this.fields, function ( f: Field ) {
      return f.name == fieldName;
    });
  }

  function findWhere<T>( values: Iterable<T>, cond: T->Bool) : T {
    for( v in values ) {
      if( cond(v) ) {
        return v;
      }
    }

    return null;
  }
}
