package hxgiven;

import haxe.Constraints;

@:generic
class BaseFixture<T:Constructible<Void->Void>> extends AbstractKeyworded<T>{
  var steps:T;

  public function new() {
    super();
  }

  public function getSteps() : T {
    if( this.steps == null ) {
      this.steps = new T();
    }

    return this.steps;
  }

  function resetSteps() : Void {
    this.steps = new T();
  }

  override function getInstance() : T {
    return getSteps();
  }
}
