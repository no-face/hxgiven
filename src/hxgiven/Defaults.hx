package hxgiven;

import hxgiven.report.ReportLevel;
import hxgiven.report.Printer;
import hxgiven.Reporter;

class Defaults{
  public static var reportLevel:ReportLevel = Minimum;

  public static var buildReporter : Void->Reporter = function () : Reporter {
    return new hxgiven.report.ConsoleReporter();
  };

  public static var buildPrinter : Void->Printer = function () : Printer {
    return new hxgiven.report.SimplePrinter();
  };
}
